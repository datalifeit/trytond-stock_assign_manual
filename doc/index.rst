Stock Assign Manual Module
##########################

The Stock Assign Manual module adds a wizard on shipments and production that
allows you to decide from which precise location to pick products.

A second wizard allows to unassign the whole or a specific quantity
from inventory moves.
